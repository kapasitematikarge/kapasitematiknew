﻿<h3>"Operatör Analiz" Ekranı Artık Tablet ve Mobilde!</h3>

<p style="text-align: left">

  <img src="/images/haberler/oee-analizi-kapasitematik-1.png" alt="oee-analizi-kapasitematik-1" width="60%" style="float:left;padding:0px 15px 0px 0px;">

Gerçek zamanlı veri analiz ve raporlama sistemi KAPASİTEMATİK yazılımının, firmalara özel tablet ve mobil uygulaması yayınlandı. Operatör analizleri ekranının tablet versiyonu ile gerekli geri bildirimleri anlık olarak alabilirsiniz. Örneğin duruş bilgilerini anlık geri dönüşler ile kayıt altına alabilir ve gerçek analizler doğrultusunda raporlama yapabilirsiniz. Ayrıca yeni tablet versiyonumuzda veri analizlerini, ayar girişlerini ve kayıt bilgilerinizi süre, kayıt bilgisi, kayıt açıklamaları ile çoğaltabilirsiniz. Kapasitematik Gerçek zamanlı API uygulaması ile ERP,CRM entegrasyonlarını çift yönlü olarak entegre edebilir ; planlama ve iş süreçlerinizi yönetebilirsiniz. 
Kapasitematik yeni API’si  ile piyasadaki tüm kabul görmüş ERP,CRM ve üretim planlama uygulamaları ile sorunsuz entegreli olarak çalışabilmekte ve geri bildirimler sağlayabilmektedir.

</p>


<div>

  <p style="float:left;">
    <a class="example-image-link" data-lightbox="example-1" href="https://www.tezmaksan.com.tr/img/screen3.png">
      <img alt="image-1" class="example-image" src="https://www.tezmaksan.com.tr/img/screen3.png" style="margin-top:5px;" width="400">&nbsp; </a>
  </p>

  <p style="float:left;">
    <a class="example-image-link" data-lightbox="example-1" href="https://www.tezmaksan.com.tr/img/SCREEN2.png">
      <img alt="image-1" class="example-image" src="https://www.tezmaksan.com.tr/img/SCREEN2.png" style="margin-top:5px;" width="400">&nbsp; </a>
  </p>

</div>
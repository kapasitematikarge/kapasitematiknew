﻿<div class="col-md-12 text-left">	
                            <p class="text-left ">
                                <span>
								</span></p><h1 title="KAPASİTEMATİK 2018 ile Endüstri 4.0 cebinizde" style="font-size: 24px;line-height: 28px; margin-bottom: 0px !important;" class="kalinyazi ogz_baslikrengi">
								KAPASİTEMATİK 2018 ile Endüstri 4.0 cebinizde								</h1>
								<h2 style="font-size:20px; line-height:22px; " class="kalinyazi ogz_baslikrengi"></h2>								
								
								
							
																
																		
								<span class="text-left prg_aciklama" style="font-size:16px; line-height:24px; color:#666;"><p><strong>İnovasyon ve İleri Teknoloji Müdürü Özer Cem Kelahmet: “KAPASİTEMATİK, tam anlamıyla Endüstri 4.0’ın kendisi… ‘Nesnelerin interneti’ alt yapısı ile veriler, tüm ortamlardan kayıpsız izlenip makinalar arası iletişim sağlanmaktadır. KAPASİTEMATİK 2018’in son güncellemesi ile tüm işlemler, akılı telefonlarda uygulama olarak markette yerini alacak.”</strong></p>

<p><strong>Özer Cem Kelahmet</strong><br>
TEZMAKSAN<br>
İnovasyon ve İleri Teknoloji Müdürü</p>

<p><strong>Kısaca özetler misiniz? KAPASİTEMATİK’in amacı nedir?</strong><br>
KAPASİTEMATİK ile firmaların, makinalarında verimlilik analizi yapabilmesi ve bu analizler ile birlikte makinaların toplam iş hacmi sürelerinin artırılması sağlanmaktadır. KAPASİTEMATİK, bir çeşit üretim takip sistemidir ve kullanıcının üretimin tüm aşamalarını takip etmesine ve doğru veri analizlerini yapmasına olanak sağlar.</p>

<p><strong>KAPASİTEMATİK kullanıcılarını 2018’de hangi güncellemeler bekliyor?</strong><br>
Firmalar, 2018 yılı ilk çeyreğinden itibaren KAPASİTEMATİK ile birlikte veri kayıpları olmaksızın makinalarında verimlik analizleri yapabilecekler. Anlık olarak verilerin akışlarını izleyip hangi makinanın, hangi saniyede, kaç adet işlem hacminde olduğunu; anlık grafikler ile izleyebilecek. Böylece kullanıcı dostu ara yüzle, uluslararası bir standartta üretim takibi sağlanacak.Endüstri 4.0 standartlarına uygun olarak IOT ve “makina öğrenmesi” algoritması ile makinaların birbiriyle olan bağlantısı<br>
sağlanmaktadır. Bu “makina öğrenmesi” ile “büyük veri” dediğimiz verilen iş, veren için anlamlı sonuçlar üreterek üretim faaliyetlerinin artırılması sağlanacaktır. KAPASETİMETİK ile Web/Ipad /Mobil gibi tüm ortamlarda verilere anlık ulaşılabilecek ve makina istatistikleri yine anlık olarak tüm platformlarda izlenebilecek. Ayrıca KAPASİTEMATİK 2018’in son güncellemesi ile tüm işlemler, akıllı telefonlarda uygulama olarak markette yerini alacak. Yani üretim mobil olarak da her ortamdan güvenli olarak izlenip müdahale edilebilecek. KAPASİTEMATİK’in tüm işlemleri ve platformu, bulut ortamında tam güvenli şekilde müşterilere özel olarak tutulacaktır.</p>

<p><strong>KAPASİTEMATİK kullanan bir firma hangi avantajlarla karşılaşır?</strong><br>
Kapasitematik kullanıcıları:<br>
1- Verimlik analizlerini uluslararası standartlarda yapabilecek.<br>
2- Tüm verileri anlık olarak izleyebilecek.<br>
3- Verileri, “anlamlı sonuçlar halinde” anlık olarak grafiklerle takip edebilecek.<br>
4- “Makina öğrenmesi” ile makinalar arası iletişim sağlayacak.<br>
5- Tüm platformlar ile anlık olarak “çoklu kanal” şeklinde izleyebilecek.<br>
6- Tam kapasiteli raporlama analizleri yapabilecek ve geriye dönük analizleri de takip edilebilecek.</p>

<p><strong>KAPASİTEMATİK ile Endüstri 4.0’ı bağdaştırabilir misiniz?</strong><br>
KAPASİTEMATİK tam anlamıyla bir Endüstri 4.0 uygulamasıdır. Bu uygulama ile birlikte büyük veriler, ön analiz işlemleriyle kayıt altında tutulmaktadır. “Nesnelerin interneti” alt yapısı ile veriler, tüm ortamlardan kayıpsız izlenip makinalar arası iletişim sağlanmaktadır. Bu da KAPASİTEMATİK’in, Endüstri 4.0’ı tam olarak amaçlayan ve yeni sanayi devrimine uyumlu bir sistem olduğunu gösteriyor.</p>

<p>&nbsp;</p></span>
								
								                            
                                                        	 
                                                             
								
                                
								
								<br>
                            <p></p>
                        </div>
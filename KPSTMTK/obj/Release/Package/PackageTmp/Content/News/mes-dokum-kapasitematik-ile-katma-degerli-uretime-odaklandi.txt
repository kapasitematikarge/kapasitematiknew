﻿<h3> MES DÖKÜM, KAPASİTEMATİK İLE KATMA DEĞERLİ ÜRETİME ODAKLANDI</h3>
<p>
  <strong>MES Döküm Üretim Müdürü Adnan Öznam: “Verimli çalışıyor muyuz acaba? Bunu kim sorguluyor? Kim, neyi, ne kadar zamanda yapıyor? Birçok işletmede bu durum tam anlamı ile analiz edilemiyor. İşte bu noktada KAPASİTEMATİK bizlere yardımcı olabilecek bir uygulama… Bu yüzden TEZMAKSAN’ı, sanayicinin soru ve sorunlarına cevap veren konularda yaptığı atılımlar için ayrıca tebrik ediyorum.”</strong>
 ​<img align="center" height="400" src="http://www.machinetoolexpress.com/images/78/kaps.png" style="margin:6px;" weight="200px">
</p>
<p>
  <strong>KAPASİTEMATİK kullanmaya nasıl karar verdiniz?</strong>
  <br>
Firmamız gün geçtikçe büyümektedir. Sürekli olarak tezgah ve personel yatırımı yapıyoruz. Makina ve personel sayımız artmış durumda… Takip etmekte zorlanmaya başladık. İşte bu noktada makinaların ve personelin eş zamanlı olarak verimliliklerini takip etmemiz gerekliydi. Günümüzde artık birim maliyet çok önemli. Bir ürünü en kısa sürede, hatasız üreterek; katma değerimizi artıracağımızın bilincindeyiz. Bu yüzden KAPASİTEMATİK kullanmaya karar verdik. Bu sayede Makina ve Personel verimliliğini takip etmiş olacağız. Özetle “vakit, nakittir.”
</p>

<p>
  <strong>KAPASİTEMATİK kullanarak hangi avantajları yakalamayı planlıyorsunuz?</strong>
  <br>
    KAPASİTEMATİK ile birim maliyetini hesaplamak daha kolay olacak. “Makina ve Personel Kapasite Durumu” net verilerle gözlemlenebilecek. Böylece KAPASİTEMATİK, katma değerle müşteri siparişlerinin teslim sürelerinde de avantaj sağlayacak. İş ve personel takibini de KAPASİTEMATİK ile sistematik olarak gerçekleştireceğiz.<br>
      <strong>Büyük ölçekli ve makine parkuru yoğun olan üreticiler için Kapasitematik yerinde bir uygulama mı?</strong>
      <br>
        Ülkemiz sanayisindeki en büyük eksikliklerden biri bu yöndeydi. “Verimli çalışıyor muyuz acaba?” Bunu kim sorguluyor? Kim, neyi, ne kadar zamanda yapıyor? Birçok işletmede bu durum bence tam anlamı ile analiz edilemiyor. İşte bu noktada KAPASİTEMATİK bizlere yardımcı olabilecek bir uygulama… Bu yüzden TEZMAKSAN’ı, sanayicinin soru ve sorunlarına cevap veren konularda yaptığı atılımlar için ayrıca tebrik ediyorum.<br>
          <strong>KAPASİTEMATİK’in sizi Endüstri 4.0’a bir adım daha yaklaştırdığına inanıyor musunuz?&nbsp;</strong>
          <br>
Bilindiği üzere firmamızın Amiral gemisi Yılmaz Redüktör... ELK Motor ve Mes Döküm bir Yılmaz Redüktör kuruluşudur. Zaten işletmelerimizde otomasyon ve robotik sistemler son dönemde hızlı bir biçimde kullanılmaktadır. Patronlarımız bu yönde bizlere gereken desteği veriyor. Çağ teknoloji çağı... Endüstri 4.0 için gerekli altyapı çalışmalarımız hızla devam ediyor. KAPASİTEMATİK, bu noktada büyük ölçekli firmalara fayda sağlayacaktır.
</p>
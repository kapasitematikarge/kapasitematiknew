﻿using KPSTMTK.Models;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace KPSTMTK.Business
{
    public class NewsletterManager
    {

        public bool AddNewSubscriber(string subsriberMailAddress)
        {
            int result = 0;

            if (SubscriberIsExists(subsriberMailAddress))
            {
                kpssiteEntities db = new kpssiteEntities();

                Newsletter newsletter = new Newsletter();

                string eMail = Sanitizer.GetSafeHtmlFragment(subsriberMailAddress);

                if (Utilities.IsEmailValidFormat(eMail))
                {
                    newsletter.Email = eMail;
                }
                else
                {
                    throw new Exception("email format error");
                }

                newsletter.CreateDate = DateTime.Now;
                db.News.Add(newsletter);
                result = db.SaveChanges();


            }

            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        // TODO: Bu kısım validation metodu olarak remote validation gibi de kullanılabilir.
        private bool SubscriberIsExists(string subsriberMailAddress)
        {
            kpssiteEntities db = new kpssiteEntities();

            var subscriber = db.News.FirstOrDefault(x => x.Email == subsriberMailAddress);

            if (subscriber != null)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
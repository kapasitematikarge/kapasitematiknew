﻿
using KPSTMTK.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace KPSTMTK.Business
{
    public class NewsManager
    {
        public List<News> GetNewsFromJson(string path)
        {
            List<News> newsList = new List<News>();
            
            using (StreamReader streamReader = new StreamReader(path))
            {
                string json = streamReader.ReadToEnd();
                newsList = JsonConvert.DeserializeObject<List<News>>(json);
            }

            return newsList;
        }

        public string GetNewsDetailFromFile(string path)
        {

            string newsDetail;
            try
            {
                using (StreamReader streamReader = new StreamReader(path))
                {
                    newsDetail = streamReader.ReadToEnd();

                }
            }
            catch
            {

                newsDetail = string.Empty;
            }


            return newsDetail;
        }

    }
}
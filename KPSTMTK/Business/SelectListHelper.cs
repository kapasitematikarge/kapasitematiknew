﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Web.Mvc;
using KPSTMTK.Models;

namespace KPSTMTK.Business
{
    
 

public class SelectListHelper
    {

        public List<SelectListItem> CityNameList(string SelectedCityCode,string filePath)
        {
            Dictionary<string, string> cityNameListDictionary;

            using (StreamReader streamReader = new StreamReader(filePath))
            {
                string json = streamReader.ReadToEnd();
                cityNameListDictionary = JsonConvert.DeserializeObject<Dictionary<string,string>>(json);
            }
         
            List<SelectListItem> CityNameList = cityNameListDictionary.Select(x => new SelectListItem { Value = x.Key, Text = x.Value, Selected = SelectedCityCode == x.Key.ToString() }).ToList();


            //SelectListItem selectListItem;

            //foreach (var city in cityNameListDictionary)
            //{
            //    selectListItem = new SelectListItem();
            //    selectListItem.Text = city.Value;
            //    selectListItem.Value = city.Key;
            //    selectListItem.Selected = SelectedCity == city.Key.ToString();
            //    CityNameList.Add(selectListItem);
            //}

            return CityNameList;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using KPSTMTK.Models;

namespace KPSTMTK.Business
{
    public class MailManager
    {
        public bool SendMail(string subject, string body,string language)
        {
            bool result = false;

            var emailList = ConfigurationManager.AppSettings["sendMail"];
            NetworkCredential MailCredential = new NetworkCredential();
            MailCredential.UserName = ConfigurationManager.AppSettings["emailAccount"];
            MailCredential.Password = ConfigurationManager.AppSettings["emailPassword"];

            SmtpClient MailClient = new SmtpClient();
            MailClient.Host = ConfigurationManager.AppSettings["emailHost"];
            MailClient.Port = 587;
            MailClient.EnableSsl = false;
            MailClient.Credentials = MailCredential;

            //MailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            var tos = emailList.Split(';');
            MailAddress sender = new MailAddress(ConfigurationManager.AppSettings["emailAccount"], "Kapasitematik talebi var");
            MailMessage MailMsg = new MailMessage();
            MailMsg.From = sender;

            for (int i = 0; i < tos.Length; i++)
            {
                if (!string.IsNullOrEmpty(tos[i]))
                    MailMsg.To.Add(tos[i]);
            }
            MailMsg.Subject = subject;
            MailMsg.IsBodyHtml = true;
            MailMsg.Body = body;

            try
            {
                MailClient.Send(MailMsg);
                result = true;
            }
            catch (Exception ex)
            {
                // TODO Log error!
                result = false;
            }

            return result;
        }

    }
}
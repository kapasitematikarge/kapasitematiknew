﻿
using KPSTMTK.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace KPSTMTK.Business
{
    public class ReferanceManager
    {
        public List<Referances> GetReferanceFromJson(string path)
        {
            List<Referances> referanceList = new List<Referances>();

            using (StreamReader streamReader = new StreamReader(path))
            {
                string json = streamReader.ReadToEnd();
                referanceList = JsonConvert.DeserializeObject<List<Referances>>(json);
            }

            return referanceList;
        }

        public List<Referances> GetReferanceFromJsonEn(string path)
        {
            List<Referances> referanceListEn = new List<Referances>();

            using (StreamReader streamReader = new StreamReader(path))
            {
                string json = streamReader.ReadToEnd();
                referanceListEn = JsonConvert.DeserializeObject<List<Referances>>(json);
            }

            return referanceListEn;
        }

        public string GetReferanceDetailFromFile(string path)
        {

            string referanceDetail;
            try
            {
                using (StreamReader streamReader = new StreamReader(path))
                {
                    referanceDetail = streamReader.ReadToEnd();

                }
            }
            catch
            {

                referanceDetail = string.Empty;
            }


            return referanceDetail;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPSTMTK.Models
{
    public class News
    {
        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Content")]
        public string Content { get; set; }

        [JsonProperty("Url")]
        public string Url { get; set; }

        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("ListThumb")]
        public string ListThumb { get; set; }


    }
 
}
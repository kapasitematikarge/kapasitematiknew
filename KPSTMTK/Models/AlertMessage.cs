﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPSTMTK.Models
{
    public enum AlertMessageLevel
    { Danger, Success }
    public class AlertMessage
    {
        public AlertMessageLevel Level { get; set; }
        public string MessageText { get; set; }

        public string RedirectUrl { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPSTMTK.Models
{
    public class Rss
    {
        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Aciklama { get; set; }
        public string Link { get; set; }
        public string Tarih { get; set; }
    }
}
﻿using KPSTMTK.Properties;
using KPSTMTK.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using KPSTMTK.Models;
using KPSTMTK.Business;

namespace KPSTMTK.Controllers
{
    public class HaberlerController : Controller
    {

        [Route("Haberler")]
        public ActionResult NewsList()
        {
            NewsManager newsManager = new NewsManager();

            var newsFiltered = newsManager.GetNewsFromJson(Server.MapPath("~/content/news/haberler.json"));

            return View(newsFiltered);

        }
        
        [Route("Haberler/{newstitle}")]
        public ActionResult NewsPanel(string newstitle)
        {
            News news = new News();
            string newsTitleFormatted = newstitle.Replace("-", "_").ToLower();

            ViewBag.Title = newstitle.Replace("-", " ");

            NewsManager newsManager = new NewsManager();

            string sourceRaw = newsManager.GetNewsDetailFromFile(Server.MapPath("~/content/News/"+newstitle+".txt"));

            if (!string.IsNullOrEmpty(sourceRaw))
            {
                news.Content = sourceRaw;
            }
            else
            {
                news.Content = "Bu içerik bulunmamaktadır. Lütfen adresin doğru yazıldığından emin olun.";
            }

            news.Title = newstitle.Replace("-", " ").ToUpper();

            return View(news);
        }
        private static string GetResourceTitle<T>(string key)
        {
            ResourceManager rm = new ResourceManager(typeof(T));
            string someString = rm.GetString(key);
            return someString;
        }
    }
   
   


}



/*
    //selectListHelper.CityNameList("0", Server.MapPath("~/CityNames.json"));


            //List<News> newsList = new List<News>();

            //News news;
            //news= new News();
            //news.Title = "\"OPERATÖR ANALİZ\" EKRANI ARTIK TABLET VE MOBİL DE!";
            //news.Content = "içerik yüklenmedi";
            //news.Url = "haberler/Operator-Analiz-ekrani-artik-tablet-ve-mobilde";
            //news.Date = "21 Aralık 2018";
            //news.ListThumb = "/images/haberler/oee-analizi-kapasitematik-1.png";
            //newsList.Add(news);

            //news = new News();
            //news.Title = "MES DÖKÜM, KAPASİTEMATİK İLE KATMA DEĞERLİ ÜRETİME ODAKLANDI";
            //news.Content = "içerik yüklenmedi";
            //news.Url = "haberler/mes-dokum-kapasitematik-ile-katma-degerli-uretime-odaklandi";
            //news.Date = "21 Aralık 2018";
            //news.ListThumb = "http://www.machinetoolexpress.com/images/78/kaps.png";
            //newsList.Add(news);

            //news = new News();
            //news.Title = "KAPASİTEMATİK İLE ÜRETİM ALANINIZDAN HİÇ KOPMUYORSUNUZ";
            //news.Content = "içerik yüklenmedi";
            //news.Url = "haberler/kapasitematik-ile-uretim-alaninizdan-hic-kopmuyorsunuz";
            //news.Date = "21 Aralık 2018";
            //news.ListThumb = "http://www.machinetoolexpress.com/resim/c3.png";
            //newsList.Add(news);

            //news = new News();
            //news.Title = "KAPASİTEMATİK KULLANICISI, ENDÜSTRİ 4.0’I CEBİNDE TAŞIYOR";
            //news.Content = "içerik yüklenmedi";
            //news.Url = "haberler/kapasitematik-kullanicisi-endustri-40i-cebinde-tasiyor";
            //news.Date = "21 Aralık 2018";
            //news.ListThumb = "http://www.machinetoolexpress.com/upload/kapasitematik-kullanicisi-endustri-40i-cebinde-tasiyor-149448541290-b.jpg";
            //newsList.Add(news);





            //news = new News();
            //news.Title = "demo dmeo";
            //news.Content = "içerik yüklenmedi";
            //news.Url = "haberler/kapasitematik-kullanicisi-endustri-40i-cebinde-tasiyor";
            //news.Date = "21 Aralık 2018";
            //news.ListThumb = "http://www.machinetoolexpress.com/upload/kapasitematik-kullanicisi-endustri-40i-cebinde-tasiyor-149448541290-b.jpg";
            //newsList.Add(news);
*/

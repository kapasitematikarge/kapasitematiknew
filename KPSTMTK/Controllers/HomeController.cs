﻿using KPSTMTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using KPSTMTK.Business;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;

namespace KPSTMTK.Controllers
{

    /// <summary>
    /// Local
    /// </summary>
    public class HomeController : Controller
    {

        kpssiteEntities db = new kpssiteEntities();

        [Route("")]
        [Route("Index")]
        [Route("Anasayfa")]
        public ActionResult Index()
        {
            var selectListHelper = new SelectListHelper();
            ViewBag.CityNames = selectListHelper.CityNameList("0", Server.MapPath("~/Content/CityNames.json"));
            return View();
        }

        [Route("en")]
         
        public ActionResult IndexEn()
        {
            return View();
        }
        [Route("gizlilik-politikasi")]
        public ActionResult GizlilikPolitikasi()
        {
            return View();
        }
        [Route("privacy-policy")]
        public ActionResult PrivacyPolicy()
        {
            return View();
        }
        [Route("cerez-politikasi")]
        public ActionResult CerezPolitikasi()
        {
            return View();
        }
        [Route("aydinlatma-beyani", Name = "aydinlatma-beyani")]
        public ActionResult AydinlatmaBeyani()
        {
            return View();
        }
        [Route("veri-politikasi", Name = "veri-politikasi")]
        public ActionResult VeriPolitikasi()
        {

            return View();

        }

        [Route("kisisel-verilerin-korunmasi-ve-islenmesi-politikasi", Name = "kisisel-verilerin-korunmasi-ve-islenmesi-politikasi")]
        public ActionResult KisiselVeriKorunmasiIslenmesiPolitikasi()
        {

            return View();

        }
        public ActionResult _musteriAydinlatmaBeyani()
        {

            return PartialView();
        }
        public ActionResult _webSitesiSosyalMedyaAydinlatmaBeyani()
        {

            return PartialView();
        }


        [Route("cookie-policy")]
        public ActionResult CookiePolicy()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult News(string newsletterSubscriber,string language)
        {
            language = language.ToLower();

            try
            {
                
                NewsletterManager newsletterManager = new NewsletterManager();

                bool subscriptionResult=  newsletterManager.AddNewSubscriber(newsletterSubscriber);

                if (subscriptionResult)
                {
                    throw new Exception();
                }

                switch (language)
                {
                    case "tr": TempData["AlertMessage"] = Resources.ResourceTr.NewsletterSuccess; break;
                    case "en": TempData["AlertMessage"] = Resources.ResourceEn.NewsletterSuccess; break;
                }
            }
            catch (Exception ex)
            {
                switch (language)
                {
                    case "tr": TempData["AlertMessage"] = Resources.ResourceTr.NewsletterError; break;
                    case "en": TempData["AlertMessage"] = Resources.ResourceEn.NewsletterError; break;
                }

                //TODO log error
            }

            return RedirectByLanguage(language);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Demo(Demo demo,string language)
        {



            language = language.ToLower();

            var response = Request["g-recaptcha-response"];
            const string secret = "	6LfM-IMUAAAAAIHxEX6qX5lFvFirsjqWFfKZJeUP";

            var client = new WebClient();
            var reply = client.DownloadString(
            string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            if (captchaResponse.Success)
            {

                if (ModelState.IsValid)
                {


                    MailManager mailManager = new MailManager();

                    string mailBody = "Eposta: " + demo.Email + " <br>" + " Ad Soyad : " + demo.Name + " <br>" + " Şirket Adı: " + demo.CompanyName + " <br>" + " Şirket Telefonu: " + demo.Phone + " <br>" + " Telefon: " + demo.MobilPhone + " <br>" + " İl: " + demo.City;

                    bool mailSentResult = false;

                    try
                    {
                        mailSentResult = mailManager.SendMail("Kapasitematik Talep Formu", mailBody, language);
                    }
                    catch
                    {
                        //TODO log
                        return RedirectByLanguage(language);
                    }


                    if (mailSentResult)
                    {
                        switch (language)
                        {
                            case "tr": TempData["AlertMessage"] = Resources.ResourceTr.DemoRequestSuccess; break;
                            case "en": TempData["AlertMessage"] = Resources.ResourceEn.DemoRequestSuccess; break;
                        }
                    }
                    else
                    {
                        switch (language)
                        {
                            case "tr": TempData["AlertMessage"] = Resources.ResourceTr.DemoRequestError; break;
                            case "en": TempData["AlertMessage"] = Resources.ResourceEn.DemoRequestError; break;
                        }
                    }



                    try
                    {
                        var talep = new Demo
                        {
                            Email = Sanitizer.GetSafeHtmlFragment(demo.Email),
                            CompanyName = Sanitizer.GetSafeHtmlFragment(demo.CompanyName),
                            Name = Sanitizer.GetSafeHtmlFragment(demo.Name),
                            Phone = Sanitizer.GetSafeHtmlFragment(demo.Phone),
                            City = Sanitizer.GetSafeHtmlFragment(demo.City),
                            MobilPhone = Sanitizer.GetSafeHtmlFragment(demo.MobilPhone),
                            CreateDate = DateTime.Now,
                            Kvkk_Onay = demo.Kvkk_Onay == "on" ? "true" : "false",
                        };

                        db.Demo.Add(talep);

                        //TODO REMOVE
                        if (Properties.Settings.Default.IsDbActive)
                        {
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // TODO Log error!
                    }

                }
            }
            else
            {
                switch (language)
                {
                    case "tr": TempData["AlertMessage"] = Resources.ResourceTr.CaptchaMessage; break;
                    case "en": TempData["AlertMessage"] = Resources.ResourceEn.CaptchaMessage; break;
                }

            }

                return RedirectByLanguage(language);
        }
        [Route("iletisim")]
        public ActionResult Contact()
        {
            return View();
        }
        [Route("mobil-uygulama")]
        public ActionResult App()
        {
            return View();
        }
        [Route("kapasitematik-talep-formu")]
        public ActionResult TalepFormu()
        {
            var selectListHelper = new SelectListHelper();
            ViewBag.CityNames = selectListHelper.CityNameList("0", Server.MapPath("~/Content/CityNames.json"));
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(Mail model,string language)
        {
            language = language.ToLower();

            var response = Request["g-recaptcha-response"];
            const string secret = "6LfM-IMUAAAAAIHxEX6qX5lFvFirsjqWFfKZJeUP";

            var client = new WebClient();
            var reply = client.DownloadString(
            string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            if (captchaResponse.Success)
            {

                if (ModelState.IsValid)
                {
                    MailManager mailManager = new MailManager();
                    string mailBody = "Eposta: " + model.Email + " <br>" + " Ad Soyad : " + model.Name + " <br>" + " Mesaj: " + model.Message;


                    bool mailSentResult = false;

                    try
                    {
                        mailSentResult = mailManager.SendMail("Kapasitematik iletişim formu", mailBody, language);
                    }
                    catch
                    {
                        //TODO log
                        return RedirectByLanguage(language);
                    }


                    if (mailSentResult)
                    {
                        switch (language)
                        {
                            case "tr": TempData["AlertMessage"] = Resources.ResourceTr.ContactUsSuccess; break;
                            case "en": TempData["AlertMessage"] = Resources.ResourceEn.ContactUsSuccess; break;
                        }
                    }
                    else
                    {
                        switch (language)
                        {
                            case "tr": TempData["AlertMessage"] = Resources.ResourceTr.ContactUsError; break;
                            case "en": TempData["AlertMessage"] = Resources.ResourceEn.ContactUsError; break;
                        }
                    }

                    try
                    {
                        var mail = new Mail
                        {
                            Email = Sanitizer.GetSafeHtmlFragment(model.Email),
                            Message = Sanitizer.GetSafeHtmlFragment(model.Message),
                            Name = Sanitizer.GetSafeHtmlFragment(model.Name),
                            CreateDate = DateTime.Now,
                            Kvkk_Onay = model.Kvkk_Onay == "on" ? "true" : "false",
                        };
                        db.Mail.Add(mail);


                        //TODO REMOVE
                        if (Properties.Settings.Default.IsDbActive)
                        {
                            db.SaveChanges();
                        }

                    }
                    catch (Exception)
                    {
                        // TODO log
                    }
                }

            }
            else
            {
                switch (language)
                {
                    case "tr": TempData["AlertMessage"] = Resources.ResourceTr.CaptchaMessage; break;
                    case "en": TempData["AlertMessage"] = Resources.ResourceEn.CaptchaMessage; break;
                }
                
            }
          
            return RedirectByLanguage(language);
        }


        
        #region Blog Pages    

        [Route("Blog")]
        public ActionResult BlogTemp()
        {
            TempData["AlertMessage"] = "Blog sayfamız kısa bir süre sonra hizmetinize girecektir!";
            return RedirectToAction("index");
        }


       //  [Route("Blog")]
        public ActionResult Blog()
        {
            return View();
        }

        [Route("Blog2")]
        public ActionResult Blog2
            ()
        {
            return View();
        }
        [Route("BlogPage")]
        public ActionResult BlogPage()
        {
            return View();
        }
        [Route("BlogPage2")]
        public ActionResult BlogPage2()
        {
            return View();
        }
        [Route("BlogPage3")]
        public ActionResult BlogPage3()
        {
            return View();

        }
        [Route("BlogPage4")]
        public ActionResult BlogPage4()
        {
            return View();

        }
        [Route("BlogPage5")]
        public ActionResult BlogPage5()
        {
            return View();
        }

        #endregion

        public class CaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("error-codes")]
            public List<string> ErrorCodes { get; set; }
        }


        public ActionResult RedirectByLanguage(string language)
        {
            language = language.ToLower();

            switch (language)
            {
                case "tr": return RedirectToRoute("");
                case "en": return RedirectToAction("indexen", "home"); // RedirectToRoute("en");
                default: return RedirectToRoute("");
            }
            
        }

        [Route("rss")]

        public ActionResult Rss()
        {
            RssOlusturucu olustur = new RssOlusturucu();
            olustur.Olustur();
            return View();
        }

        [Route("eng-rss")]

        public ActionResult EngRss()
        {
            RssOlusturucu olustur = new RssOlusturucu();
            olustur.EngOlustur();
            return View();
        }

    }
}
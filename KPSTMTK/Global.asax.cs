﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace KPSTMTK
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        //#if RELEASE#endif 
        protected void Application_BeginRequest()
        {

            //TODO Localhost denetimi silinecek
            if (Request.Url.Host != "localhost")
            {


                if (Request.Url.Scheme == "http")
                {
                    string path;

                    if (Request.Url.Host.ToLower().StartsWith("www"))
                    {
                        path = "https://" + Request.Url.Host + Request.Url.PathAndQuery;

                    }
                    else
                    {
                        path = "https://www." + Request.Url.Host + Request.Url.PathAndQuery;
                    }

                    Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", path);
                }
                else if (Request.Url.Scheme == "https")
                {
                    string path;

                    if (!Request.Url.Host.ToLower().StartsWith("www"))
                    {
                        path = "https://www." + Request.Url.Host + Request.Url.PathAndQuery;
                        Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", path);
                    }


                }
                
            }
        }


    }
}

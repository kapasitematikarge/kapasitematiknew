﻿var cevrimZamani = 0;
var uretimAdet = 0;
var hurdaAdet = 0;
var vardiyaSuresi = 0;
var planliDurus = 0;
var plansizDurus = 0;

// Readonly hesaplanan alanlar
var netKullanilabilirSaat = 0;
var netOperasyonSaati = 0;
var tumUretimSaati = 0;
var hurdaKayipSaati = 0;

// Sonuc
var verimlilik = 0;
var kullanilabilirlik = 0;
var kalite = 0;
var oee = 0;

$("#oeelbl").text("%0");

$("#cevrimZamani").on("input", function (e) {
    cevrimZamani = $('#cevrimZamani').val();

    OeeHesapla();
});

$("#uretimAdet").on("input", function (e) {

    uretimAdet = $('#uretimAdet').val();
    OeeHesapla();
});

$("#hurdaAdet").on("input", function (e) {
    hurdaAdet = $('#hurdaAdet').val();
    OeeHesapla();
});

$("#vardiyaSuresi").on("input", function (e) {
    vardiyaSuresi = $('#vardiyaSuresi').val();
    OeeHesapla();
});

$("#planliDurus").on("input", function (e) {
    planliDurus = $('#planliDurus').val();
    OeeHesapla();
});

$("#plansizDurus").on("input", function (e) {
    plansizDurus = $('#plansizDurus').val();
    OeeHesapla();
});


function OeeHesapla() {

    netKullanilabilirSaat = (60 * vardiyaSuresi) - planliDurus;
    netOperasyonSaati = netKullanilabilirSaat - plansizDurus;
    tumUretimSaati = ((uretimAdet * cevrimZamani) / 60).toFixed(2);
    hurdaKayipSaati = ((hurdaAdet * cevrimZamani) / 60).toFixed(2);

    // Sonuc
    kullanilabilirlik = (netOperasyonSaati / netKullanilabilirSaat).toFixed(2);
    verimlilik = (tumUretimSaati / netOperasyonSaati).toFixed(2);
    kalite = ((tumUretimSaati - hurdaKayipSaati) / tumUretimSaati).toFixed(2);

    oee = ((kullanilabilirlik * verimlilik * kalite) * 100).toFixed(2);

    $("#netKullanilabilirSaat").val(netKullanilabilirSaat);
    $("#netOperasyonSaati").val(netOperasyonSaati);
    $("#tumUretimSaati").val(tumUretimSaati);
    $("#hurdaKayipSaati").val(hurdaKayipSaati);

    $("#kullanilabilirlik").val((kullanilabilirlik * 100).toFixed(2));
    $("#verimlilik").val((verimlilik * 100).toFixed(2));
    $("#kalite").val((kalite * 100).toFixed(2));

    //$("#oee").val(oee);

    $("#oeelbl").text("% "+noNaN(percentage(oee, 100)) );

    $("#progressbar").css("width", percentage(oee, 100) + "%");
}

function percentage(num, per) { return (num / 100) * per; }

function noNaN(n) { return isNaN(n) ? 0 : n; }